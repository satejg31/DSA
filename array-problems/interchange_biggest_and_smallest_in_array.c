#include <stdio.h>
#include <stdlib.h>

#define SUCCESS 	1
#define FAILURE		0

typedef int status_t;

status_t find_largest_num(int *p_arr, int arr_size ,int *largest);
status_t find_smallest_num(int *p_arr, int arr_size, int *smallest);

int main(void)
{
        int *p_arr = NULL;
        int arr_size = 0;
	int i = 0;
	int temp_val = 0;
	int large_index  = -1;
	int small_index = -1;
	status_t retval;

        printf("Enter the size of the array : ");
        scanf("%d", &arr_size);

        p_arr = (int *)malloc(sizeof(int) * arr_size + i);
        if(p_arr == NULL)
        {
                fprintf(stderr, "ERROR : Memory allocation failed");
                exit(EXIT_FAILURE);
        }

        for(i = 0; i < arr_size; i++)
        {
                printf("p_arr[%d] = ", i);
                scanf("%d", (p_arr+i));
        }
	
	retval = find_largest_num(p_arr, arr_size, &large_index);
	if(retval == FAILURE)	
	{
		fprintf(stderr, "ERROR : In finding the largest number\n");
		exit(EXIT_FAILURE);
	}

	retval = find_smallest_num(p_arr, arr_size, &small_index);
	if(retval == FAILURE)	
	{
		fprintf(stderr, "ERROR : In finding the smallest number\n");
		exit(EXIT_FAILURE);
	}
	
	temp_val = p_arr[large_index];
	p_arr[large_index] = p_arr[small_index];
	p_arr[small_index] = temp_val;

	puts("Printing array after interchanging the largest and smallest input : ");
        for(i = 0; i < arr_size; i++)
        {
                printf("p_arr[%d] = %d\n", i, p_arr[i]);
        }
	
	free(p_arr);
	p_arr = NULL;

	exit(EXIT_SUCCESS);
}

status_t find_largest_num(int *p_arr, int arr_size, int *largest)
{	
	int num = 0;
	int pos = -1;
	for(int i =0;i < arr_size; i++) 
	{
		if(p_arr[i] > num)
		{
			num = p_arr[i];
			pos = i;
		}
	}
	if(pos == -1)
		return (FAILURE);

	*largest = pos;
	return (SUCCESS);
}

status_t find_smallest_num(int *p_arr, int arr_size, int *smallest)
{
	int num = p_arr[0];
	int pos = -1;
	
	for(int i=0;i < arr_size; i++)
	{
		if(p_arr[i] <= num)
		{
			num = p_arr[i];
			pos = i;
		}
	}

	if(pos == -1)
		return (FAILURE);

	*smallest = pos;
	return (SUCCESS);
}

