/*
 *
 * 	@ AUTHOR : SATEJ UPENDRA GANJI
 * 	@ DATE : 02/04/2024
 * 	@ PURPOSE : Print the mean of all the numbers in the array
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int *p_arr = NULL;
        int arr_size = 0;
        int i = 0;
	int sum = 0;

        printf("Enter the size of the array : ");
        scanf("%d", &arr_size);

        p_arr = (int *)malloc(sizeof(int) * arr_size);
        if(p_arr == NULL)
        {
                fprintf(stderr, "ERROR : Memory allocation failed");
                exit(EXIT_FAILURE);
        }

        for(i = 0; i < arr_size; i++)
        {
                printf("p_arr[%d] = ", i);
                scanf("%d", (p_arr+i));
        }
	
	printf("Showing the created array : \n");
	for(i = 0;i < arr_size; i++)
	{
		printf("p_arr[%d] = %d\n", i, *(p_arr + i));
		sum += *(p_arr + i);
	}

	printf("The mean of all the numbers entered in the array is as follows : %f\n", ((float)sum / (float)arr_size));

	free(p_arr);
	p_arr = NULL;

	exit(EXIT_SUCCESS);
}

