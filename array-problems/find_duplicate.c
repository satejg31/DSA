#include <stdio.h>
#include <stdlib.h>

int main(void)
{
        int *p_arr = NULL;
        int arr_size = 0;
        int i = 0, j = 0;
	int flag = 0;

        printf("Enter the size of the array : ");
        scanf("%d", &arr_size);

        p_arr = (int *)malloc(sizeof(int) * arr_size);
        if(p_arr == NULL)
        {
                fprintf(stderr, "ERROR : Memory allocation failed");
                exit(EXIT_FAILURE);
        }

        for(i = 0; i < arr_size; i++)
        {
                printf("p_arr[%d] = ", i);
                scanf("%d", (p_arr+i));
        }
	
	for(i=0;i < arr_size; i++)
	{
		for(j =i+1; j < arr_size; j++)
			if(p_arr[i] == p_arr[j])
				flag = 1;
	}
	
	if(flag == 1)
		printf("Duplicate elements found in the array\n");
	else
		printf("Duplicate elements not found\n");

	free(p_arr);
	p_arr = NULL;

	exit(EXIT_SUCCESS);
}
