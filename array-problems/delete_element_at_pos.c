#include <stdio.h>
#include <stdlib.h>

int main(void)
{
        int *p_arr = NULL;
        int arr_size = 0;
        int i = 0, j = 0;
        int num = 0;

        printf("Enter the size of the array : ");
        scanf("%d", &arr_size);

        p_arr = (int *)malloc(sizeof(int) * arr_size + i);
        if(p_arr == NULL)
        {
                fprintf(stderr, "ERROR : Memory allocation failed");
                exit(EXIT_FAILURE);
        }

        for(i = 0; i < arr_size; i++)
        {
                printf("p_arr[%d] = ", i);
                scanf("%d", (p_arr+i));
        }
	
	printf("Enter the location from which you need to delete the element : ");
	scanf("%d", &num);

	if(num >= arr_size)
	{
		fprintf(stderr, "ERROR : Please enter the valid index to delete.");
		scanf("%d", &num);
	}
	
	for(i = num; i < arr_size - 1; i++)
		p_arr[i] = p_arr[i+1];

	printf("Array after deleting : \n");
	for(i = 0;i < arr_size -1; i++)
		printf("p_arr[%d] = %d\n", i, p_arr[i]);

	free(p_arr);
	p_arr = NULL;

	exit(EXIT_SUCCESS);
}
