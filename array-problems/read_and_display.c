#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int *p_arr = NULL;
	int arr_size = 0;
	int i = 0;

	printf("Enter the size of the array : ");
	scanf("%d", &arr_size);
	
	p_arr = (int *)malloc(sizeof(int) * arr_size);
	if(p_arr == NULL)
	{
		fprintf(stderr, "ERROR : Memory allocation failed");
		exit(EXIT_FAILURE);
	}

	for(i = 0; i < arr_size; i++)
	{
		printf("p_arr[%d] = ", i);
		scanf("%d", (p_arr+i));
	}

	puts("Printing the array : ");
	for(i = 0;i < arr_size; i++)
		printf("p_arr[%d] = %d\n", i, *(p_arr + i));
	
	free(p_arr);
	p_arr = NULL;

	exit(EXIT_SUCCESS);
}

