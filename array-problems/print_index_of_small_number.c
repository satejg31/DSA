#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int *p_arr = NULL;
	size_t arr_size = 0;
	size_t i = 0;
	int small = 0;
	int small_index = -1;

	printf("Enter the size of array : ");
	scanf("%ld", &arr_size);

	p_arr = (int *)malloc(sizeof(int) * arr_size);
	if(p_arr == NULL)
	{
		fprintf(stderr, "Error in memory allocation");
		exit(EXIT_SUCCESS);
	}

	for(i=0; i < arr_size; i++)
	{
		printf("p_arr[%ld] = ", i);
		scanf("%d", (p_arr + i));
	}
	
	small = *(p_arr + 0);
	for(i = 1; i < arr_size; i++)
	{
		if(small > p_arr[i])
		{
			small = p_arr[i];	
			small_index = i;
		}
	}
	
	printf("Small array entry index is : %d\n", small_index);

	free(p_arr);
	p_arr = NULL;

	exit(EXIT_SUCCESS);
}
