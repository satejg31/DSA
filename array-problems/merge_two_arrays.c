#include <stdio.h>
#include <stdlib.h>

#define TRUE 		1

int main(void)
{
        int *p_arr1 = NULL;
	int *p_arr2 = NULL;
        int *p_arr_merged = NULL;
	int arr_size1 = 0;
	int arr_size2 = 0;

        int i = 0, j = 0, k = 0;

        printf("Enter the size of the array1 : ");
        scanf("%d", &arr_size1);

        printf("Enter the size of the array2 : ");
        scanf("%d", &arr_size2);	

        p_arr1 = (int *)malloc(sizeof(int) * arr_size1);
        if(p_arr1 == NULL)
        {
                fprintf(stderr, "ERROR : Memory allocation failed");
                exit(EXIT_FAILURE);
        }

        for(i = 0; i < arr_size1; i++)
        {
                printf("p_arr1[%d] = ", i);
                scanf("%d", (p_arr1+i));
        }

        p_arr2 = (int *)malloc(sizeof(int) * arr_size2);
        if(p_arr2 == NULL)
        {
                fprintf(stderr, "ERROR : Memory allocation failed");
                exit(EXIT_FAILURE);
        }

        for(i = 0; i < arr_size2; i++)
        {
                printf("p_arr2[%d] = ", i);
                scanf("%d", (p_arr2+i));
        }

	p_arr_merged = (int *)malloc(sizeof(int) * arr_size1 + arr_size2);
	if(p_arr_merged == NULL)
	{
		fprintf(stderr, "ERROR : Memory allocation failed");
		exit(EXIT_FAILURE);
	}
	
	i = 0;
	j = 0;
	k = 0;

	while(TRUE)
	{
		if(i == arr_size1)
		{
			while(j < arr_size2)
			{
				p_arr_merged[k] = p_arr2[j];
				j++;
				k++;
			}
			break;		
		}
		
		if(j == arr_size2)
		{
			while(i < arr_size1)
			{
				p_arr_merged[k] = p_arr1[i];
				i++;
				k++;
			}
			break;
		}

		if(p_arr1[i] < p_arr2[j])
		{
			p_arr_merged[k] = p_arr1[i];
			i++;
			k++;
		}
		else
		{
			p_arr_merged[k] = p_arr2[j];
			j++;
			k++;
		}
	}
	
	printf("Printing the merged array : \n");
	for(i = 0;i < arr_size1 + arr_size2; i++)
		printf("p_arr_merged[%d] = %d\n", i, p_arr_merged[i]);

	free(p_arr1);
	p_arr1 = NULL;

	free(p_arr2);
	p_arr2 = NULL;

	free(p_arr_merged);
	p_arr_merged = NULL;

	exit(EXIT_SUCCESS);

}

