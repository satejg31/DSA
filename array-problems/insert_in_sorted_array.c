#include <stdio.h>
#include <stdlib.h>

int main(void)
{
        int *p_arr = NULL;
        int arr_size = 0;
        int i = 0, j = 0;
	int num = 0;

        printf("Enter the size of the array : ");
        scanf("%d", &arr_size);

        p_arr = (int *)malloc(sizeof(int) * arr_size + i);
        if(p_arr == NULL)
        {
                fprintf(stderr, "ERROR : Memory allocation failed");
                exit(EXIT_FAILURE);
        }

        for(i = 0; i < arr_size; i++)
        {
                printf("p_arr[%d] = ", i);
                scanf("%d", (p_arr+i));
        }
		
	printf("Enter a number which is to be inserted : ");
	scanf("%d", &num);
	
	for(int i =0;i < arr_size; i++)
	{
		if(p_arr[i] > num)
		{
			for(int j=arr_size - 1; j >= i; j--)
				p_arr[j+1] = p_arr[j];
			p_arr[i] = num;
			break;
		}
	}
	
	printf("Printing the array : \n");
	for(i=0; i<arr_size + 1; i++)
		printf("p_arr[%d] = %d\n", i, p_arr[i]);

	free(p_arr);
	p_arr = NULL;

	exit(EXIT_SUCCESS);
}

