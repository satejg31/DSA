#include <stdio.h>
#include <stdlib.h>

int main(void)
{
        int *p_arr = NULL;
        size_t arr_size = 0;
        size_t i = 0;
	int large1 = 0;
       	int large2 = 0;

        printf("Enter the size of array : ");
        scanf("%ld", &arr_size);

        p_arr = (int *)malloc(sizeof(int) * arr_size);
        if(p_arr == NULL)
        {
                fprintf(stderr, "Error in memory allocation");
                exit(EXIT_SUCCESS);
        }

        for(i=0; i < arr_size; i++)
        {
                printf("p_arr[%ld] = ", i);
                scanf("%d", (p_arr + i));
        }
	
	large1 = p_arr[0];
	large2 = p_arr[1];

	for(i = 1; i < arr_size; i++)
	{
		if(large1 < p_arr[i])
		{
			large2 = large1;
			large1 = p_arr[i];
	
		}
	}	
	
	printf("Second Largest number from the array is : %d\n", large2);

	free(p_arr);
	p_arr = NULL;

	exit(EXIT_SUCCESS);
}
