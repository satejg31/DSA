#include <stdio.h>
#include <stdlib.h>

int main(void)
{
        int *p_arr = NULL;
        int arr_size = 0;
        int i = 0;
	int pos = 0;
	int num = 0;
	int *p_resultant = NULL;

        printf("Enter the size of the array : ");
        scanf("%d", &arr_size);

        p_arr = (int *)malloc(sizeof(int) * arr_size);
        if(p_arr == NULL)
        {
                fprintf(stderr, "ERROR : Memory allocation failed");
                exit(EXIT_FAILURE);
        }

        for(i = 0; i < arr_size; i++)
        {
                printf("p_arr[%d] = ", i);
                scanf("%d", (p_arr+i));
        }
	
	printf("Enter the position at which you need insert element : ");
	scanf("%d", &pos);

	if(pos >= arr_size)
	{
		printf("Please enter a valid input for the insertion position\n");
		scanf("%d", &pos);
	}

	printf("Enter the value you need to enter on given position : ");
	scanf("%d", &num);
	
	p_resultant = (int* )malloc(sizeof(int) * arr_size + 1);
	if(p_resultant == NULL)
	{
		fprintf(stderr, "ERROR : Memory allocation failed");
		exit(EXIT_SUCCESS);
	}

	for(i =0;i < arr_size + 1; i++)
	{
		if(i == pos)
		{
			p_resultant[i] = num;
			i++;
		}
		p_resultant[i] = p_arr[i];
	}

	free(p_arr);
	p_arr = NULL;

	printf("Printing the resultant array : \n");
	for(i =0;i < arr_size + 1; i++)
		printf("p_resultant[%d] = %d\n", i, p_resultant[i]);

	free(p_resultant);
	p_resultant = NULL;

	exit(EXIT_SUCCESS);
}
