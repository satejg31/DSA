/*
    @ AUTHOR : SATEJ UPENDRA GANJI
    @ DATE : 06/04/2024
    @ PURPOSE : HEADER FILE FOR THE DOUBLY CIRCULAR LINKED LIST

*/

#ifndef _DCLL_H
#define _DCLL_H

#define SUCCESS                 1
#define FAILURE                 0
#define TRUE                    1
#define FALSE                   0    
#define LIST_DATA_NOT_FOUND     2
#define LIST_EMPTY              3


typedef int len_t;
typedef int status_t;
typedef int data_t;
typedef int bool;
typedef struct __node __node_t;
typedef __node_t list_t;

struct __node 
{
    struct __node *next;
    struct __node *prev;
    data_t data;
};

list_t *create_list(void);

status_t insert_begin(list_t *p_list, data_t new_data);
status_t insert_end(list_t *p_list, data_t new_data);
status_t insert_after(list_t *p_list, data_t e_data, data_t new_data);
status_t insert_before(list_t *p_list, data_t e_data, data_t new_data);

status_t get_beginning(list_t *p_list, data_t *p_data);
status_t get_end(list_t *p_list, data_t *p_data);
status_t pop_beginning(list_t *p_list, data_t *p_popped_data);
status_t pop_end(list_t *p_list, data_t *p_popped_data);

status_t remove_beginning(list_t *p_list);
status_t remove_end(list_t *p_list);
status_t remove_data(list_t *p_list, data_t remove_data);

list_t *concat_lists(list_t *p_list1, list_t *p_list2);
list_t *merge_lists(list_t *p_list1, list_t *p_list2);
status_t append_lists(list_t *p_list1, list_t *p_list2);
list_t *get_reversed_list(list_t *p_list);
status_t reverse_list(list_t *p_list);

status_t destroy_list(list_t **pp_list);

status_t list_to_array(list_t *p_list, data_t **pp_array, size_t *p_arr_size);
list_t *array_to_list(data_t *p_array, size_t arr_size);

len_t get_list_length(list_t *p_list);
bool is_list_empty(list_t *p_list);
bool contains(list_t *p_list, data_t search_data);
int get_repeat_count(list_t *p_list, data_t search_data);
void show_list(list_t *p_list, const char *msg);

/* List auxillary routines */

static void __generic_insert(__node_t *p_beg, __node_t *p_mid, __node_t *p_end);
static void __generic_delete(__node_t *p_delete_node);
static __node_t *__get_list_node(data_t new_data);
static __node_t *__locate_list_node(list_t *p_list, data_t search_data);

/* memory allocation techniques */

void *__xmalloc(size_t size_in_bytes);

#endif /* _DCLL_H */

