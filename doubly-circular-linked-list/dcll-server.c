/*
    @ AUTHOR : SATEJ UPENDRA GANJI
    @ DATE : 06/04/2024
    @ PURPOSE : SERVER FILE FOR THE DOUBLY CIRCULAR LINKED LIST

*/

#include <stdio.h>
#include <stdlib.h>

#include "dcll.h"

list_t *create_list(void)
{
    list_t *p_list = NULL;

    p_list = __get_list_node(0);
    p_list->next = p_list;
    p_list->prev = p_list;

    return (p_list);
}

status_t insert_begin(list_t *p_list, data_t new_data)
{
    __generic_insert(p_list, __get_list_node(new_data), p_list->next);
    return (SUCCESS);
}

status_t insert_end(list_t *p_list, data_t new_data)
{
    __generic_insert(p_list->prev, __get_list_node(new_data), p_list);
    return (SUCCESS);
}

status_t insert_after(list_t *p_list, data_t e_data, data_t new_data)
{
    __node_t *p_existing_node = NULL;
    p_existing_node = __locate_list_node(p_list, e_data);

    if(p_existing_node == NULL)
        return (LIST_DATA_NOT_FOUND);

    __generic_insert(p_existing_node, __get_list_node(new_data), p_existing_node->next);
    return (SUCCESS);
}

status_t insert_before(list_t *p_list, data_t e_data, data_t new_data)
{
    __node_t *p_existing_node = NULL;
    p_existing_node = __locate_list_node(p_list, e_data);

    if(p_existing_node == NULL)
        return (LIST_DATA_NOT_FOUND);

    __generic_insert(p_existing_node->prev, __get_list_node(new_data), p_existing_node);
    return (SUCCESS);
}

status_t get_beginning(list_t *p_list, data_t *p_data)
{
    if(is_list_empty(p_list) == TRUE)
        return (LIST_EMPTY);

    *p_data = p_list->next->data;
    return (SUCCESS);
}

status_t get_end(list_t *p_list, data_t *p_data)
{
    if(is_list_empty(p_list) == TRUE)
        return (LIST_EMPTY);

    *p_data = p_list->prev->data;
    return (SUCCESS);
}

status_t pop_beginning(list_t *p_list, data_t *p_popped_data)
{
    if(is_list_empty(p_list) == TRUE)
        return (LIST_EMPTY);

    *p_popped_data = p_list->next->data;
    __generic_delete(p_list->next);
    return (SUCCESS);
}

status_t pop_end(list_t *p_list, data_t *p_popped_data)
{
    if(is_list_empty(p_list) == TRUE)
        return (LIST_EMPTY);

    *p_popped_data = p_list->prev->data;
    __generic_delete(p_list->prev);
    return (SUCCESS);

}

status_t remove_beginning(list_t *p_list)
{
    if(is_list_empty(p_list) == TRUE)
        return (LIST_EMPTY);

    __generic_delete(p_list->next);
    return (SUCCESS);
}

status_t remove_end(list_t *p_list)
{
    if(is_list_empty(p_list) == TRUE)
        return (LIST_EMPTY);

    __generic_delete(p_list->prev);
    return (SUCCESS);
}

status_t remove_data(list_t *p_list, data_t remove_data)
{
    __node_t *p_remove_node = NULL;
    
    p_remove_node = __locate_list_node(p_list, remove_data);
    
    if(p_remove_node == NULL)
        return (LIST_DATA_NOT_FOUND);

    __generic_delete(p_remove_node);
    return (SUCCESS);
}


list_t *concat_lists(list_t *p_list1, list_t *p_list2)
{
    __node_t *p_run_node = NULL;
    list_t *p_new_list = NULL;

    p_new_list = create_list();

    for(p_run_node = p_list1->next;
        p_run_node != p_list1;
        p_run_node = p_run_node->next)
        
        insert_end(p_new_list, p_run_node->data);


    for(p_run_node = p_list2->next;
        p_run_node != p_list2;
        p_run_node = p_run_node->next)

        insert_end(p_new_list, p_run_node->data);


    return (p_new_list);
}

list_t *merge_lists(list_t *p_list1, list_t *p_list2)
{
    list_t *p_new_list = NULL;
    __node_t *p_run_node1 = NULL;
    __node_t *p_run_node2 = NULL;

    p_new_list = create_list();
    p_run_node1 = p_list1->next;
    p_run_node2 = p_list2->next;


    while(TRUE)
    {
        if(p_run_node1 == p_list1)
        {
            while(p_run_node2 != p_list2)
            {
                insert_end(p_new_list, p_run_node2->data);
                p_run_node2 = p_run_node2->next;
            }

            break;
        }

        if(p_run_node2 == p_list2)
        {
            while(p_run_node1 != p_list1)
            {
                insert_end(p_new_list, p_run_node1->data);
                p_run_node1 = p_run_node1->next;
            }

            break;
        }

        if(p_run_node1->data <= p_run_node2->data)
        {
            insert_end(p_new_list, p_run_node1->data);
            p_run_node1 = p_run_node1->next;
        }
        else
        {
            insert_end(p_new_list, p_run_node2->data);
            p_run_node2 = p_run_node2->next;
        }
    }

    return (p_new_list);
}

status_t append_lists(list_t *p_list1, list_t *p_list2)
{
    p_list1->prev->next = p_list2->next;
    p_list2->next->prev = p_list1->prev;

    p_list1->prev = p_list2->prev;
    p_list2->prev->next = p_list1;

    free(p_list2);
    return (SUCCESS);
}

list_t *get_reversed_list(list_t *p_list)
{
    list_t *p_new_list = NULL;
    __node_t *p_run_node = NULL;

    p_run_node = p_list->next;
    p_new_list = create_list();

    for(p_run_node = p_list->next;
        p_run_node != p_list;
        p_run_node = p_run_node->next)
        insert_begin(p_new_list, p_run_node->data);

    return (p_new_list);
}

status_t reverse_list(list_t *p_list)
{
    __node_t *p_run_node = NULL;
    __node_t *p_run_prev = NULL;
    __node_t *p_run_prev_orig = NULL;

    p_run_prev_orig = p_list->prev;

    for(p_run_node = p_list->prev->prev;
        p_run_node != p_list;
        p_run_node = p_run_prev)
    {
        p_run_prev = p_run_node->prev;
        __generic_insert(p_list->prev, p_run_node, p_list);
    }

    p_run_prev_orig->prev = p_list;
    p_list->next = p_run_prev_orig;

    return (SUCCESS);
}

status_t destroy_list(list_t **pp_list)
{
    list_t *p_list = *pp_list;
    __node_t *p_run_node = NULL;
    __node_t *p_run_node_next = NULL;

    for(p_run_node = p_list->next;
        p_run_node != p_list;
        p_run_node = p_run_node_next)
    {
        p_run_node_next = p_run_node->next;
        free(p_run_node);
    }

    free(p_list);
    *pp_list = NULL;

    return (SUCCESS);

}

status_t list_to_array(list_t *p_list, data_t **pp_array, size_t *p_arr_size)
{
    if(is_list_empty(p_list) == TRUE)
    {
        fprintf(stderr, "ERROR : List is empty");
        return (FAILURE);
    }
    
    data_t *p_array = NULL;
    len_t list_length = 0;
    __node_t *p_run_node = NULL;
    int i = 0;

    list_length = get_list_length(p_list);
    
    p_array = (data_t *)__xmalloc(sizeof(data_t) * list_length);
    for(i = 0, p_run_node = p_list->next;
        p_run_node != p_list;
        i++, p_run_node = p_run_node->next)

            *(p_array + i) = p_run_node->data;

    *pp_array = p_array;
    *p_arr_size = list_length;


    return (SUCCESS);
}

list_t *array_to_list(data_t *p_array, size_t arr_size)
{
    if(p_array == NULL || arr_size < 0)
    {
        fprintf(stderr, "Array is empty !!");
        return (NULL);
    }

    list_t *p_new_list = NULL;
    size_t i = 0;

    p_new_list = create_list();

    for(i = 0; i < arr_size; i++)
        insert_end(p_new_list, *(p_array + i));

    return (p_new_list);
    
}

len_t get_list_length(list_t *p_list)
{
    len_t list_length = 0;
    __node_t *p_run_node = NULL;

    for(p_run_node = p_list->next; 
        p_run_node != p_list; 
        p_run_node = p_run_node->next, list_length++);

    return (list_length);
    
}

bool is_list_empty(list_t *p_list)
{
    return (p_list->next == p_list && p_list->prev == p_list);
}

bool contains(list_t *p_list, data_t e_data)
{
    if(__locate_list_node(p_list, e_data) != NULL)
        return (TRUE);
    else
        return (FALSE);
}

int get_repeat_count(list_t *p_list, data_t search_data)
{
    __node_t *p_run_node = NULL;
    int count = 0;

    for(p_run_node = p_list->next;
        p_run_node != p_list;
        p_run_node = p_run_node->next)
    {
        if(__locate_list_node(p_list, search_data) != NULL)
            count++;
    }

    return (count);
}

void show_list(list_t *p_list, const char *msg)
{
    if(msg)
        puts(msg);

    printf("[BEG]<->");
    __node_t *p_run_node = NULL;
    for(p_run_node = p_list->next; 
        p_run_node != p_list; 
        p_run_node = p_run_node->next)
    {    
        printf("[%d]<->", p_run_node->data);
    }

    printf("[END]\n\n");
}

/* List auxillary routines */

static void __generic_insert(__node_t *p_beg, __node_t *p_mid, __node_t *p_end)
{
    p_mid->next = p_end;
    p_mid->prev = p_beg;
    p_beg->next = p_mid;
    p_end->prev = p_mid;
}

static void __generic_delete(__node_t *p_delete_node)
{
    p_delete_node->prev->next = p_delete_node->next;
    p_delete_node->next->prev = p_delete_node->prev;

    free(p_delete_node);
    p_delete_node = NULL;
}

static __node_t *__get_list_node(data_t new_data)
{
    __node_t *p_new_node = NULL;
    p_new_node = (__node_t *)__xmalloc(sizeof(__node_t));

    p_new_node->data = new_data;
    p_new_node->next = NULL;
    p_new_node->prev = NULL;

    return (p_new_node);
}

static __node_t *__locate_list_node(list_t *p_list, data_t search_data)
{
    __node_t *p_run_node = NULL;

    p_run_node = p_list->next;
    while(p_run_node != p_list)
    {
        if(p_run_node->data == search_data)
            return (p_run_node);

        p_run_node = p_run_node->next;
    }

    return (NULL);
}

/* memory allocation techniques */

void *__xmalloc(size_t size_in_bytes)
{
    void *p = NULL;
    p = malloc(size_in_bytes);

    if(p == NULL)
    {
        fprintf(stderr, "ERROR : Memory allocation failed");
        exit(EXIT_FAILURE);
    }

    return (p);
}
