/*
    @ AUTHOR : SATEJ UPENDRA GANJI
    @ DATE : 13/04/2024
    @ PURPOSE : SERVER FILE FOR THE SINGLY CIRCULAR LINKED LIST
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "scll.h"

/* List Auxillary routines */

list_t *create_list(void)
{
	__node_t *p_head_node = NULL;

	p_head_node = __get_list_node(-1);
	p_head_node->next = p_head_node;

	return (p_head_node);
}

status_t insert_beginning(list_t* p_list, data_t new_data)
{
	__generic_insert(p_list, __get_list_node(new_data), p_list->next);
	return (SUCCESS);
}

status_t insert_end(list_t* p_list, data_t new_data)
{
	__node_t *p_last_node = NULL;
	__get_last_node(p_list, &p_last_node);
	if(p_last_node == NULL)
		return (FAILURE);

	__generic_insert(p_last_node, __get_list_node(new_data), p_last_node->next);
	return (SUCCESS);
}

status_t insert_after(list_t* p_list, data_t e_data, data_t new_data)
{
	__node_t *p_existing_node = NULL;
	
	p_existing_node = __search_node(p_list, e_data);
	if(p_existing_node == NULL)
		return (LIST_DATA_NOT_FOUND);
	
	__generic_insert(p_existing_node, __get_list_node(new_data), p_existing_node->next);
	return (SUCCESS);
}

status_t insert_before(list_t* p_list, data_t e_data, data_t new_data)
{
	__node_t *p_existing_node = NULL;
	__node_t *p_prev_node = NULL;

	__get_node_and_prev(p_list, e_data, &p_existing_node, &p_prev_node);
	if(p_existing_node == NULL)
		return (LIST_DATA_NOT_FOUND);

	__generic_insert(p_prev_node, __get_list_node(new_data), p_existing_node);
	return (SUCCESS);
}

status_t get_beginning(list_t* p_list, data_t* p_data)
{
	if(is_list_empty(p_list) == TRUE)
		return (LIST_EMPTY);

	*p_data = p_list->next->data;
	return (SUCCESS);
}

status_t get_end(list_t* p_list, data_t* p_data)
{
	if(is_list_empty(p_list) == TRUE)
		return (LIST_EMPTY);
	
	__node_t *p_last_node = NULL;
	__get_last_node(p_list, &p_last_node);
	if(p_last_node)
	{
		*p_data = p_last_node->data;
		return (SUCCESS);
	} 
	else
		return (FAILURE); 

}

status_t pop_beginning(list_t* p_list, data_t *p_data)
{
	if(is_list_empty(p_list) == TRUE)
		return (LIST_EMPTY);

	*p_data = p_list->next->data;
	__generic_delete(p_list, p_list->next);

	return (SUCCESS);
}

status_t pop_end(list_t* p_list, data_t* p_data)
{
	if(is_list_empty(p_list) == TRUE)
		return (TRUE);
	
	__node_t *p_last_node = NULL;
	__node_t *p_last_prev_node = NULL;

	__get_last_node_and_prev(p_list, &p_last_node, &p_last_prev_node);
	*p_data = p_last_node->data;
	__generic_delete(p_last_prev_node, p_last_node);
	return (SUCCESS);
}

status_t remove_beginning(list_t* p_list)
{
	if(is_list_empty(p_list) == TRUE)
		return (LIST_EMPTY);
	
	__generic_delete(p_list, p_list->next);
	return (SUCCESS);
}

status_t remove_end(list_t* p_list)
{
	if(is_list_empty(p_list) == LIST_EMPTY)
		return (LIST_EMPTY);
	
	__node_t *p_last_node = NULL;
	__node_t *p_last_prev_node = NULL;
	
	__get_last_node_and_prev(p_list, &p_last_node, &p_last_prev_node);
	__generic_delete(p_last_prev_node, p_last_node);
	return (SUCCESS);
}	

status_t remove_data(list_t* p_list, data_t remove_data)
{
	__node_t *p_data_node = NULL;
	__node_t *p_data_prev_node = NULL;

	__get_node_and_prev(p_list, remove_data, &p_data_node, &p_data_prev_node);
	if(p_data_node == NULL)
		return (LIST_DATA_NOT_FOUND);
	
	__generic_delete(p_data_prev_node, p_data_node);
	return (SUCCESS);
}

list_t* concat_lists(list_t* p_list1, list_t* p_list2)
{
	list_t *p_new_list = NULL;
	__node_t *p_run_node = NULL;

	p_new_list = create_list();
	
	p_run_node = p_list1->next;
	while(p_run_node != p_list1)
	{
		insert_end(p_new_list, p_run_node->data);
		p_run_node = p_run_node->next;
	}

	p_run_node = p_list2->next;
	while(p_run_node != p_list2)
	{
		insert_end(p_new_list, p_run_node->data);
		p_run_node = p_run_node->next;
	}

	return (p_new_list);	
}

list_t* merge_lists(list_t* p_list1, list_t* p_list2)
{
	list_t *p_new_list = NULL;
	__node_t *p_run_node1 = NULL;
	__node_t *p_run_node2 = NULL;

	p_run_node1 = p_list1->next;
	p_run_node2 = p_list2->next;

	p_new_list = create_list();

	while(TRUE)
	{
		if(p_run_node1 == p_list1)
		{
			while(p_run_node2 != p_list2)
			{
				insert_end(p_new_list, p_run_node2->data);
				p_run_node2 = p_run_node2->next;
			}
			break;
		}
		
		if(p_run_node2 == p_list2)
		{
			while(p_run_node1 != p_list1)
			{
				insert_end(p_new_list, p_run_node1->data);
				p_run_node1 = p_run_node1->next;
			}
			break;
		}

		if(p_run_node1->data <= p_run_node2->data)
		{
			insert_end(p_new_list, p_run_node1->data);
			p_run_node1 = p_run_node1->next;
		}
		else
		{
			insert_end(p_new_list, p_run_node2->data);
			p_run_node2 = p_run_node2->next;
		}
	}

	return (p_new_list);
}

status_t append_lists(list_t *p_list1, list_t *p_list2)
{
	__node_t *p_last_node_of_p_list1 = NULL;
	__node_t *p_last_node_of_p_list2 = NULL;

	__get_last_node(p_list1, &p_last_node_of_p_list1);
	__get_last_node(p_list2, &p_last_node_of_p_list2);

	p_last_node_of_p_list1->next = p_list2->next;
	p_last_node_of_p_list2->next = p_list1;

	free(p_list2);
	return (SUCCESS);
}

list_t* get_reversed_list(list_t *p_list)
{
	list_t *p_new_list = NULL;
	__node_t *p_run_node = NULL;

	p_new_list = create_list();
	p_run_node = p_list->next;

	while(p_run_node != p_list)
	{
		insert_beginning(p_new_list, p_run_node->data);
		p_run_node = p_run_node->next;
	}

	return (p_new_list);
}

status_t reverse_list(list_t *p_list)
{
	__node_t *p_run_node = NULL;
	__node_t *p_run_next_node = NULL;

	if(p_list->next == NULL || p_list->next->next == NULL)
		return (SUCCESS);

	p_run_node = p_list->next->next;
	p_list->next->next = p_list;

	while(p_run_node != p_list)
	{
		p_run_next_node = p_run_node->next;
		__generic_insert(p_list, p_run_node, p_list->next);
		p_run_node = p_run_next_node;
	}

	return (SUCCESS);
}

status_t destroy_list(list_t** pp_list)
{
	__node_t *p_run_node = NULL;
	__node_t *p_run_next_node = NULL;

	list_t *p_list = *pp_list;
	p_run_node = p_list->next;
	while(p_run_node != p_list)
	{
		p_run_next_node = p_run_node->next;
		free(p_run_node);
		p_run_node = p_run_next_node;
	}

	free(p_list);
	*pp_list = NULL;

	return (SUCCESS);

}

status_t list_to_array(list_t* p_list, data_t **pp_array, size_t *p_arr_size)
{
	data_t *p_array = NULL;
	len_t list_length = 0;
	__node_t *p_run_node = NULL;
	int i = 0;

	list_length = get_list_length(p_list);
	if(list_length == 0)
		return (LIST_EMPTY);
	
	p_array = (data_t *)__xmalloc(sizeof(data_t) * list_length);
	p_run_node = p_list->next;
	while(p_run_node != p_list)
	{
		p_array[i] = p_run_node->data;
		i++;
		p_run_node = p_run_node->next;
	}

	*pp_array = p_array;
	*p_arr_size = list_length;

	return (SUCCESS);
}

list_t* array_to_list(data_t *p_array, size_t arr_size)
{
	list_t *p_new_list = NULL;
	int i;

	p_new_list = create_list();
	for(i =0;i < arr_size; i++)
		insert_end(p_new_list, p_array[i]);
	
	return (p_new_list);
}

len_t get_list_length(list_t *p_list)
{
	__node_t *p_run_node = NULL;
	len_t list_length = 0;

	p_run_node = p_list->next;
	while(p_run_node != p_list)
	{
		++list_length;
		p_run_node = p_run_node->next;
	}

	return (list_length);
}

bool is_list_empty(list_t* p_list)
{
	return (p_list->next == NULL);
}

bool contains(list_t* p_list, data_t s_data)
{
	__node_t *p_search_node = NULL;
	p_search_node = __search_node(p_list, s_data);
	if(p_search_node == NULL)
		return (FALSE);
	else
		return (TRUE);
}

void show_list(list_t* p_list, const char* msg)
{
	__node_t *p_run_node = NULL;

	if(msg)
		puts(msg);

	printf("[START]->");
	p_run_node = p_list->next;
	
	while(p_run_node != p_list)
	{
		printf("[%d]->", p_run_node->data);
		p_run_node = p_run_node->next;
	}

	puts("[END]\n");
}

int get_repeat_count(list_t* p_list, data_t repeat_data)
{
	int element_count = 0;
	__node_t *p_run_node = NULL;

	p_run_node = p_list->next;
	while(p_run_node != p_list)
	{
		if(p_run_node->data == repeat_data)
			element_count++;

		p_run_node = p_run_node->next;
	}

	return (element_count);
}

static void __generic_insert(__node_t* p_beg, __node_t* p_mid, __node_t* p_end)
{
    p_beg->next = p_mid;
    p_mid->next = p_end;
}

static void __generic_delete(__node_t* p_prev_node, __node_t* p_delete_node)
{
    p_prev_node->next = p_delete_node->next;
    free(p_delete_node);
    p_delete_node = NULL;
}

static void __get_last_node(list_t* p_list, __node_t** pp_last_node)
{
    __node_t* p_run_node = NULL;
    p_run_node = p_list;

    while(p_run_node->next != p_list)
        p_run_node = p_run_node->next;

    *pp_last_node = p_run_node;
}

static void __get_node_and_prev(list_t* p_list, data_t search_data, __node_t** pp_node, __node_t** pp_prev_node)
{
	__node_t *p_run_node = NULL;
	__node_t *p_run_node_prev = NULL;

	p_run_node = p_list->next;
	p_run_node_prev = p_list;

	while(p_run_node != p_list)
	{
		if(p_run_node->data == search_data)
		{
			*pp_node = p_run_node;
			*pp_prev_node = p_run_node_prev;
			return;
		}

		p_run_node_prev = p_run_node;
		p_run_node = p_run_node->next;
	
	}

	*pp_node = NULL;
	*pp_prev_node = NULL;
}

static void __get_last_node_and_prev(list_t* p_list, __node_t** pp_last_node, __node_t** pp_prev_node)
{
	__node_t *p_run_node = NULL;
	__node_t *p_run_node_prev = NULL;

	p_run_node = p_list;
	p_run_node_prev = NULL;

	while(p_run_node->next != p_list)
	{
		p_run_node_prev = p_run_node;
		p_run_node = p_run_node->next;
	}

	*pp_last_node = p_run_node;
	*pp_prev_node = p_run_node_prev;
}

static __node_t* __search_node(list_t* p_list, data_t search_data)
{
	__node_t *p_run_node = NULL;
	p_run_node = p_list->next;

	while(p_run_node != p_list)
	{
		if(p_run_node->data == search_data)
		{
			return (p_run_node);
		}

		p_run_node = p_run_node->next;
	}

	return (NULL);
}

static __node_t* __get_list_node(data_t new_data)
{
	__node_t *p_new_node = NULL;
	p_new_node = (__node_t *)__xmalloc(sizeof(__node_t));

	p_new_node->data = new_data;
	p_new_node->next = NULL;

	return (p_new_node);
}

/* Memory allocation functions */

static void* __xmalloc(size_t size_in_bytes)
{
    void *p = NULL;

    p = malloc(size_in_bytes);
    if(p == NULL)
    {
        fprintf(stderr, "ERROR : Memory allocation failed");
        return (NULL);
    }

    return (p);
}

