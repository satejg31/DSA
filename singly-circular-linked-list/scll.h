/*
    @ AUTHOR : SATEJ UPENDRA GANJI
    @ DATE : 13/04/2024
    @ PURPOSE : HEADER FILE FOR THE SINGLY CIRCULAR LINKED LIST
*/

#ifndef __SCLL_H
#define __SCLL_H

#define TRUE                    1
#define FALSE                   0
#define SUCCESS                 1
#define FAILURE                 0
#define LIST_EMPTY              2
#define LIST_DATA_NOT_FOUND     3

typedef int status_t;
typedef int data_t;
typedef struct __node __node_t;
typedef __node_t list_t;
typedef unsigned long size_t;
typedef int bool;
typedef int len_t;

struct __node
{
    data_t data;
    struct __node* next;
};

list_t *create_list(void);

status_t insert_beginning(list_t* p_list, data_t new_data);
status_t insert_end(list_t* p_list, data_t new_data);
status_t insert_after(list_t* p_list, data_t e_data, data_t new_data);
status_t insert_before(list_t* p_list, data_t e_data, data_t new_data);

status_t get_beginning(list_t* p_list, data_t* p_data);
status_t get_end(list_t* p_list, data_t* p_data);
status_t pop_beginning(list_t* p_list, data_t *p_data);
status_t pop_end(list_t* p_list, data_t* p_data);

status_t remove_beginning(list_t* p_list);
status_t remove_end(list_t* p_list);
status_t remove_data(list_t* p_list, data_t remove_data);

list_t* concat_lists(list_t* p_list1, list_t* p_list2);
list_t* merge_lists(list_t* p_list1, list_t* p_list2);
status_t append_lists(list_t *p_list1, list_t* p_list2);
list_t* get_reversed_list(list_t* p_list);
status_t reverse_list(list_t *p_list);

status_t destroy_list(list_t** pp_list);

status_t list_to_array(list_t* p_list, data_t **pp_array, size_t *p_arr_size);
list_t* array_to_list(data_t *p_array, size_t arr_size);

len_t get_list_length(list_t *p_list);
bool is_list_empty(list_t* p_list);
bool contains(list_t* p_list, data_t s_data);
void show_list(list_t* p_list, const char* msg);
int get_repeat_count(list_t* p_list, data_t repeat_data);

/* List Auxillary routines */

static void __generic_insert(__node_t* p_beg, __node_t* p_mid, __node_t* p_end);
static void __generic_delete(__node_t* p_prev_node, __node_t* p_delete_node);
static void __get_last_node(list_t* p_list, __node_t** pp_last_node);
static void __get_node_and_prev(list_t* p_list, data_t search_data, __node_t** pp_node, __node_t** pp_prev_node);
static void __get_last_node_and_prev(list_t* p_list, __node_t** pp_last_node, __node_t** pp_prev_node);
static __node_t* __search_node(list_t* p_list, data_t search_data);
static __node_t* __get_list_node(data_t new_data);

/* Memory allocation functions */

static void* __xmalloc(size_t size_in_bytes);

#endif /*__SCLL_H */
