/*
    @ AUTHOR : SATEJ UPENDRA GANJI
    @ DATE : 13/04/2024
    @ PURPOSE : CLIENT SIDE FOR THE SINGLY CIRCULAR LINKED LIST
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "scll.h"


int main()
{
	list_t* p_list = NULL;
	list_t* p_list1 = NULL;
	list_t* p_list2 = NULL;
	list_t* concated_list = NULL;
	list_t* merged_list = NULL;
	list_t* reversed_list = NULL;

	data_t data;
	status_t status;
	len_t length;

	p_list = create_list();

	show_list(p_list, "Showing List : ");

	printf("\n\t\t\tINSERT ROUTINES\n\n");

	for (data = 0; data < 5; ++data)
	{
		status = insert_beginning(p_list, data);
		assert(status);
	}
	show_list(p_list, "After inserting elements at start :");

	
	for (data = 5; data < 10; ++data)
	{
		status = insert_end(p_list, data);
		assert(status);
	}
	puts("\n");
	show_list(p_list, "After inserting elements at end :");

	status = insert_after(p_list, 0, 100);
	assert(status);
	puts("\n");
	show_list(p_list, "After inserting element after 0 :");

	status = insert_before(p_list, 0, 200);
	assert(status);
	puts("\n");
	show_list(p_list, "After inserting element before 0 : ");

	printf("\n\t\t\tGET ROUTINES\n\n");

	status = get_beginning(p_list, &data);
	assert(status);
	printf("Start data = %d\n", data);

	status = get_end(p_list, &data);
	assert(status);
	printf("End data = %d\n", data);

	printf("\n\t\t\tPOP ROUTINES\n\n");

	status = pop_beginning(p_list, &data);
	assert(status);
	printf("Start data = %d\n", data);
	show_list(p_list, "After popping the start element of the list : ");

	status = pop_end(p_list, &data);
	assert(status);
	puts("\n");
	printf("End data = %d\n", data);
	show_list(p_list, "After popping the end element of the list : ");

	printf("\n\t\t\tREMOVE ROUTINES\n\n");

	status = remove_beginning(p_list);
	assert(status);
	show_list(p_list, "After removing the start element of the list : ");

	status = remove_end(p_list);
	assert(status);
	puts("\n");
	show_list(p_list, "After removing the end element of the list : ");

	status = remove_data(p_list, 0);
	assert(status);
	puts("\n");
	show_list(p_list, "After removing the data element of the list : ");

	printf("\n\t\t\tLENGTH AND CONTAINS ROUTINES\n\n");

	length = get_list_length(p_list);
	printf("Length of linked list : %d\n", length);

	printf("Doee list contain the 100 element : ");
	if(contains(p_list, 100) == TRUE)
		printf("Given data is present in the linked list\n");
	else
		printf("Given data is not present in the linked list\n");

	printf("\n\t\t\tCONCATE, MERGE AND REVERSE ROUTINES\n\n");

	p_list1 = create_list();
    p_list2 = create_list();

    for(data = 5; data <= 55; data += 10)
    {
        insert_end(p_list1, data);
        insert_end(p_list2, data+5);
    }

    show_list(p_list1, "List1:");
	puts("\n");
    show_list(p_list2, "List2:");

	concated_list = concat_lists(p_list1, p_list2);
	puts("\n");
	show_list(concated_list, "Concatenated version of List1 and List2:");


	merged_list = merge_lists(p_list1, p_list2);
	puts("\n");
	show_list(merged_list, "Merged version of List1 and List2:");


	reversed_list = get_reversed_list(p_list1);
	puts("\n");
	show_list(reversed_list, "Reversed version of List1:");

    printf("\n\t\t\tAPPEND AND REVERSE INPLACE ROUTINES\n\n");

	status = append_lists(p_list1, p_list2);
	assert(status);
	show_list(p_list1, "List2 is appended behind List1:");

    show_list(p_list1, "List2 before undergoing reversal:");
    status = reverse_list(p_list1);
	assert(status);
	show_list(p_list1, "Reversed version of List2:");

    printf("\n\t\t\tARRAY TO LIST AND LIST TO ARRAY ROUTINES\n\n");

	data_t *p_array = NULL;
    size_t p_size;
    show_list(p_list, "Showing List : ");
	list_to_array(p_list, &p_array, &p_size);
    int i;
	puts("Showing List to array : ");
	for(i = 0; i < p_size; i++)
	{
		printf("p_array[%d] : %d\n", i, p_array[i]);
	}

	list_t *p_new_list = NULL;
	data_t p_array1[] = {10, 20, 30, 40, 50};

	int size = 5;
	p_new_list = array_to_list(p_array1, size);
	assert(p_new_list != NULL);
	puts("\n");
	show_list(p_new_list, "Showing Array to list : ");

    printf("\n\t\t\tDESTROYING ALL LISTS\n\n");

	status = destroy_list(&p_list);
    assert(status == SUCCESS && p_list == NULL);

	status = destroy_list(&p_list1);
    assert(status == SUCCESS && p_list1 == NULL);

	status = destroy_list(&concated_list);
    assert(status == SUCCESS && concated_list == NULL);

	status = destroy_list(&merged_list);
    assert(status == SUCCESS && merged_list == NULL);

	status = destroy_list(&reversed_list);
    assert(status == SUCCESS && reversed_list == NULL);

	status = destroy_list(&p_new_list);
    assert(status == SUCCESS && p_new_list == NULL);

	printf("\t\t\tALL LISTS ARE DESTROYED\n\n");
	return 0;
}


